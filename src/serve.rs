use clap;
use notify::{RecursiveMode, Watcher};
use std::{path::Path, sync::mpsc, thread, time::Duration};

use crate::build;
use crate::build::BuildArgs;
use crate::config::SimiConfig;
use crate::error::*;

#[derive(Clone)]
pub struct ServeArgs {
    pub build: BuildArgs,
    pub port: u16,
    pub serve_only: bool,
}

impl ServeArgs {
    pub fn from_clap<'a>(matches: &clap::ArgMatches<'a>) -> Self {
        let build = BuildArgs::from_clap(matches);
        let port: u16 = match matches.value_of("port") {
            Some(port) => port.parse().expect("Unable to parse port number"),
            _ => crate::DEFAULT_SERVE_PORT,
        };
        Self {
            build,
            port,
            serve_only: matches.is_present("only"),
        }
    }
}

pub fn run(arg: ServeArgs) -> Result<(), Error> {
    let cwd = ::std::env::current_dir().expect("unable to get current working directory");
    let config = SimiConfig::from_serve(arg.clone())?;
    let (sfs, port) = if arg.serve_only {
        let sfs = crate::simple_static_server::StaticFiles::new(&cwd)?;
        (sfs, arg.port)
    } else {
        // First build
        build::build(&config)?;

        let sfs = crate::simple_static_server::StaticFiles::new(
            &config.output_path().to_string_lossy().to_string(),
        )?;
        (sfs, config.serve_port())
    };
    let server_thread = thread::spawn(move || sfs.start(port));

    watch(&cwd, &config);
    server_thread.join().unwrap();
    Ok(())
}

fn watch(cwd: &Path, config: &SimiConfig) {
    let src_dir = cwd.join("src");
    let static_dir = cwd.join("static");
    let (tx, rx) = mpsc::channel();
    let mut watcher = notify::watcher(tx, Duration::from_millis(500)).unwrap();
    watcher.watch(src_dir, RecursiveMode::Recursive).unwrap();
    if static_dir.exists() {
        watcher.watch(static_dir, RecursiveMode::Recursive).unwrap();
    }

    loop {
        match rx.recv() {
            Ok(_event) => build::build(&config).unwrap(),
            Err(err) => println!("watch error: {:?}", err),
        }
    }
}
